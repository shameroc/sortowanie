//
// Created by sh4 on 27.11.2020.
//

#ifndef SORTOWANIE_BST_H
#define SORTOWANIE_BST_H

#include "Tree.h"

class Bst : public Tree {
    friend class Avl;
    friend class RedBlack;
private:
    Node* root;
    Node* insert (Node*, Node*);
    Node* insertIncludingParent (Node*, Node*, Node*);
    void inorder (Node*);
    Node* search (Node*, int);
    Node* remove (Node*, int);

public:
    bool search (int);
    void insert (int);
    void remove (int);
    void print();
    Bst(int);
    Bst();
};


#endif //SORTOWANIE_BST_H
