#include "Sort.h"
#include "Kopiec.cpp"

void Sort::insertionSort(int* arr, int length) {
    for (int j = 1, element, i; j < length; j++) {

        element = arr[j];
        i = j - 1;

        while (i >= 0 && arr[i] > element) {
            arr[i + 1] = arr[i];
            i--;
        }

        arr[i + 1] = element;
    }
}

void Sort::scalanie (int* arr, int p, int q, int r) {
    int *arr_help = new int[r - p];
    int i = p;
    int j = q + 1;
    int k = 0;

    while (i <= q && j <= r) {
        if (arr[j] < arr[i]) {
            arr_help[k] = arr[j];
            j++;
        } else {
            arr_help[k] = arr[i];
            i++;
        }
        k++;
    }

    if (i <= q) {
        while (i <= q) {
            arr_help[k] = arr[i];
            i++;
            k++;
        }
    } else {
        while (j <= r) {
            arr_help[k] = arr[j];
            j++;
            k++;
        }
    }

    for (i = 0; i <= r - p; i++) {
        arr[p + i] = arr_help[i];
    }

    delete[] arr_help;

}

void Sort::sortowanie_przez_scalanie (int* arr, int p, int r) {
    if (p < r) {
        int q = (p + r) / 2;
        Sort::sortowanie_przez_scalanie (arr, p, q);
        Sort::sortowanie_przez_scalanie (arr, q + 1, r);
        Sort::scalanie (arr, p, q, r);
    }
}

void Sort::heapSort(int* arr, int n) {
    Kopiec::build_heap(arr, n);

    for (int i=n-1; i>0; i--) {
        std::swap(arr[0], arr[i]);
        Kopiec::heapify(arr, 0, i);
    }
}

bool Sort::check (int* arr, int length) {
    for (int i = 0; i < length - 1; i++) {
        if (arr[i] > arr[i + 1]) {
            std::cout << "Wrong number at index " << i << "\n";
            return false;
        }
    }
//    std::cout << "Array is sorted correctly\n";
    return true;
}

void Sort::print (int* arr, int length) {
    for (int i = 0; i < length; i++) {
        std::cout << arr[i] << " ";
    }
}