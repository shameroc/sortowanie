//
// Created by sh4 on 01.12.2020.
//

#ifndef SORTOWANIE_AVL_H
#define SORTOWANIE_AVL_H

#include "Tree.h"

class Avl : public Tree {
private:
    Bst* bst;
    int height(Node*);
    int max(int, int);
    Node* rightRotate (Node*);
    Node* leftRotate (Node*);
    int getBalance (Node*);

public:
    bool search (int);
    void insert (int);
    void remove (int);
    void print();
    Avl(int);
    Avl();
};


#endif //SORTOWANIE_AVL_H
