//
// Created by Krzysztof Wydrzynski on 03/11/2020.
//

#ifndef CWICZENIA_KOPIEC_H
#define CWICZENIA_KOPIEC_H


class Kopiec {
public:
    static void heapify(int*, int, int);

    static void build_heap(int*, int);

private:
    static int potomek_lewy (int);

    static int potomek_prawy (int);
};


#endif //CWICZENIA_KOPIEC_H
