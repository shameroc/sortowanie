//
// Created by sh4 on 04.01.2021.
//

#include "Structure129.h"

bool Structure129::search(int value){
    return this->arr[value].isPresent;
}

void Structure129::push(int value) {
    List::Node* node = this->list->insertFrontElement(value);
    this->arr[value].isPresent = true;
    this->arr[value].node = node;
}

void Structure129::pop() {
    int value = this->list->deleteFrontElement();
    this->arr[value].isPresent = false;
    this->arr[value].node = nullptr;
}

void Structure129::remove(int value) {
    List::Node* node = this->arr[value].node;
    this->arr[value].isPresent = false;
    this->list->deleteNode(node);
}

Structure129::Structure129(int n) {
    this->arr = new DoubleArray[n];
    this->list = new List;
}