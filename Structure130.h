//
// Created by sh4 on 04.01.2021.
//

#ifndef SORTOWANIE_STRUCTURE130_H
#define SORTOWANIE_STRUCTURE130_H


class Structure130 {
private:
    struct List {
        struct Node {
            Node *prev;
            Node *next;
            Node *min;
            int value;
        };
        Node *frontElement;

        int deleteFrontElement() {
            if (frontElement == nullptr) return -1;
            int value = frontElement->value;
            List::Node *frontElementCpy = frontElement;
            frontElement = frontElement->next;
            delete frontElementCpy;
            if (frontElement != nullptr) {
                frontElement->prev = nullptr;
            }

            return value;
        }

        Node *insertFrontElement(int value) {
            Node *newNode = new Node();
            newNode->value = value;

            if (frontElement == nullptr) {
                newNode->min = newNode;
                frontElement = newNode;
                return newNode;
            }

            if (newNode->value < frontElement->min->value) {
                newNode->min = newNode;
            } else {
                newNode->min = frontElement->min;
            }

            Node *frontElementCopy = frontElement;
            frontElement = newNode;
            newNode->next = frontElementCopy;
            frontElementCopy->prev = newNode;

            return newNode;
        }

        void upToMin() {
            Node *minNode = frontElement->min;
            if (minNode->next == nullptr && minNode->prev == nullptr) {
                deleteFrontElement();
                return;
            }
            if (minNode->next == nullptr && minNode->prev != nullptr) {
                frontElement = nullptr;
                return;
            }
            if (minNode->next != nullptr && minNode->prev == nullptr) {
                deleteFrontElement();
                return;
            }
            frontElement = minNode->next;
        }

        void print() {
            if (frontElement == nullptr) return;
            Node *nodeToPrint = frontElement;
            std::cout << frontElement->value << " ";

            while (nodeToPrint->next != nullptr) {
                nodeToPrint = nodeToPrint->next;
                std::cout << nodeToPrint->value << " ";
            }
        }
    };

    List* list;

public:

    void push(int);

    void pop();

    void upToMin();

    void print();

    Structure130();
};


#endif //SORTOWANIE_STRUCTURE130_H
