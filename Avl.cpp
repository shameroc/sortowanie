//
// Created by sh4 on 01.12.2020.
//

#include "Avl.h"

int Avl::height(Tree::Node* node) {
    if (node == nullptr) return 0;
    return node->height;
}

int Avl::max(int a, int b) {
    return (a > b) ? a : b;
}

Tree::Node* Avl::rightRotate(Tree::Node *node) {
    Tree::Node *x = node->left;
    Tree::Node *T2 = x->right;

    x->right = node;
    node->left = T2;

    node->height = max(height(node->left), height(node->right)) + 1;
    x->height = max(height(x->left), height(x->right)) + 1;

    return x;
}

Tree::Node* Avl::leftRotate(Tree::Node *node) {
    Tree::Node *y = node->right;
    Tree::Node *T2 = y->left;

    y->left = node;
    node->right = T2;

    node->height = max(height(node->left), height(node->right)) + 1;
    y->height = max(height(y->left), height(y->right)) + 1;

    return y;
}

int Avl::getBalance(Tree::Node *node) {
    if (node == nullptr) return 0;
    return height(node->left) - height(node->right);
}

bool Avl::search(int value) {
    return this->bst->search(value);
}

void Avl::insert(int value) {
    if (this->bst->root == nullptr) {
        this->bst->insert(value);
        return;
    }

    Tree::Node* node = new Node();
    node->value = value;

    bst->insert(this->bst->root, node);

    node->height = max(height(node->left), height(node->right)) + 1;

    int balance = getBalance(node);

    if (balance > 1 && value < node->left->value) {
        rightRotate(node);
        return;
    }

    if (balance < -1 && value > node->right->value) {
        leftRotate(node);
        return;
    }

    if (balance > 1 && value > node->left->value) {
        node->left = leftRotate(node->left);
        rightRotate(node);
        return;
    }

    if (balance < -1 && value < node->right->value) {
        node->right = rightRotate(node->right);
        leftRotate(node);
        return;
    }
}

void Avl::print() {
    this->bst->print();
}

void Avl::remove(int value) {
    Tree::Node* node = this->bst->remove(this->bst->root, value);

    if (node == nullptr) return;

    node->height = max(height(node->left), height(node->right)) + 1;

    int balance = getBalance(node);

    if (balance > 1 &&
        getBalance(node->left) >= 0) {
         rightRotate(node);
        return;
    }

    if (balance > 1 && getBalance(node->left) < 0) {
        node->left = leftRotate(node->left);
        rightRotate(node);
        return;
    }

    if (balance < -1 && getBalance(node->right) <= 0) {
        leftRotate(node);
        return;
    }

    if (balance < -1 && getBalance(node->right) > 0) {
        node->right = rightRotate(node->right);
        leftRotate(node);
        return;
    }
}

Avl::Avl (int value) {
    this->bst = new Bst(value);
}

Avl::Avl() {
    this->bst = new Bst();
}