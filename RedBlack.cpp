//
// Created by sh4 on 03.01.2021.
//

#include "RedBlack.h"

bool RedBlack::search(int value) {
    return this->bst->search(value);
}

void RedBlack::insert(int value) {
    if (this->bst->root == nullptr) {
        Tree::Node* node = new Tree::Node();
        this->bst->root = node;
        node->value = value;
        node->color = 0;
        return;
    }

    Tree::Node* node = new Tree::Node();
    node->value = value;
    this->bst->insertIncludingParent(this->bst->root, node);
    fixInsert(node);
}

void RedBlack::fixInsert(Tree::Node* node) {
    Tree::Node* u;
    while (node->parent->color == 1) {
        if (node->parent == node->parent->parent->right) {
            u = node->parent->parent->left;
            if (u->color == 1) {
                u->color = 0;
                node->parent->color = 0;
                node->parent->parent->color = 1;
                node = node->parent->parent;
            } else {
                if (node == node->parent->left) {
                    node = node->parent;
                    right_rotate(node);
                }
                node->parent->color = 0;
                node->parent->parent->color = 1;
                left_rotate(node->parent->parent);
            }
        } else {
            u = node->parent->parent->right;

            if (u->color == 1) {
                u->color = 0;
                node->parent->color = 0;
                node->parent->parent->color = 1;
                node = node->parent->parent;
            } else {
                if (node == node->parent->right) {
                    node = node->parent;
                    left_rotate(node);
                }
                node->parent->color = 0;
                node->parent->parent->color = 1;
                right_rotate(node->parent->parent);
            }
        }
        if (node == this->bst->root) {
            break;
        }
    }
    this->bst->root->color = 0;
}

void RedBlack::remove(Tree::Node* node, int key) {
    Tree::Node* z = nullptr;
    Tree::Node* x;
    Tree::Node* y;
    while (node != nullptr){
        if (node->value == key) {
            z = node;
        }

        if (node->value <= key) {
            node = node->right;
        } else {
            node = node->left;
        }
    }

    if (z == nullptr) {
        return;
    }

    y = z;
    int y_original_color = y->color;
//    std::cout << "z: " << z << ": " << z->value <<'\n';
//    std::cout << "z->left: " << z->left << ": " << z->left->value <<'\n';
//    std::cout << "z->right: " << z->right << ": " << z->right->value <<'\n';
    if (z->left == nullptr) {
        x = z->right;
        rbTransplant(z, z->right);
    } else if (z->right == nullptr) {
        x = z->left;
        rbTransplant(z, z->left);
    } else {
        y = z->right;
        while (y->left != nullptr) {
            y = y->left;
        }
        y_original_color = y->color;
        x = y->right;
        if (y->parent == z) {
            x->parent = y;
        } else {
            rbTransplant(y, y->right);
            y->right = z->right;
            y->right->parent = y;
        }

        rbTransplant(z, y);
        y->left = z->left;
        y->left->parent = y;
        y->color = z->color;
    }
    delete z;
    if (y_original_color == 0){
        fixRemove(x);
    }
}

void RedBlack::fixRemove(Tree::Node *x) {
    Tree::Node* s;
    while (x != this->bst->root && x->color == 0) {
        if (x == x->parent->left) {
            s = x->parent->right;
            if (s->color == 1) {
                s->color = 0;
                x->parent->color = 1;
                left_rotate(x->parent);
                s = x->parent->right;
            }

            if ((s->left != nullptr && s->right != nullptr) && (s->left->color == 0 && s->right->color == 0)) {
                s->color = 1;
            } else {
                if (s->right != nullptr && s->right->color == 0) {
                    s->left->color = 0;
                    s->color = 1;
                    right_rotate(s);
                    s = x->parent->right;
                }

                s->color = x->parent->color;
                x->parent->color = 0;
                s->right->color = 0;
                left_rotate(x->parent);
                x = this->bst->root;
            }
        } else {
            s = x->parent->left;
            if (s->color == 1) {
                s->color = 0;
                x->parent->color = 1;
                right_rotate(x->parent);
                s = x->parent->left;
            }

            if ((s->left != nullptr && s->right != nullptr) && (s->left->color == 0 && s->right->color == 0)) {
                s->color = 1;
                x = x->parent;
            } else {
                if (s->left != nullptr && s->left->color == 0) {
                    s->right->color = 0;
                    s->color = 1;
                    left_rotate(s);
                    s = x->parent->left;
                }

                s->color = x->parent->color;
                x->parent->color = 0;
                s->left->color = 0;
                right_rotate(x->parent);
                x = this->bst->root;
            }
        }
    }
    x->color = 0;
}

void RedBlack::rbTransplant(Tree::Node* u, Tree::Node* v){
    if (u->parent == nullptr) {
        this->bst->root = v;
    } else if (u == u->parent->left){
        u->parent->left = v;
    } else {
        u->parent->right = v;
    }
    v->parent = u->parent;
}

void RedBlack::left_rotate(Tree::Node* x){
    Tree::Node* y = x->right;
    x->right = y->left;
    if (y->left != nullptr) {
        y->left->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == nullptr) {
        this->bst->root = y;
    } else if (x == x->parent->left) {
        x->parent->left = y;
    } else {
        x->parent->right = y;
    }
    y->left = x;
    x->parent = y;
}

void RedBlack::right_rotate(Tree::Node* x){
    Tree::Node* y = x->left;
    x->left = y->right;
    if (y->right != nullptr) {
        y->right->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == nullptr) {
        this->bst->root = y;
    } else if (x == x->parent->right) {
        x->parent->right = y;
    } else {
        x->parent->left = y;
    }
    y->right = x;
    x->parent = y;
}

void RedBlack::remove (int value) {
    remove(this->bst->root, value);
}


void RedBlack::print() {
    this->bst->print();
}

RedBlack::RedBlack() {
    this->bst = new Bst();
}

RedBlack::RedBlack(int value) {
    this->bst = new Bst(value);
}
