//
// Created by sh4 on 04.01.2021.
//

#include "Structure128.h"

bool Structure128::search(int number) {
    if (arr[number] == 1) return 1;
    return 0;
}

void Structure128::insert(int number) {
    arr[number] = 1;
    lastIndex->insert(number);
}

int Structure128::select() {
    int number = lastIndex->get();
    arr[number] = 0;
    return number;
}

Structure128::Structure128(int n) {
    this->n = n;
    this->arr = new int[n];
    this->lastIndex = new Stack();
}

