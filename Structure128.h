//
// Created by sh4 on 04.01.2021.
//

#ifndef SORTOWANIE_STRUCTURE128_H
#define SORTOWANIE_STRUCTURE128_H


class Structure128 {
private:
    int n;
    int* arr;
    struct Stack {
    private:
        struct Node {
            Node* prev;
            int value;
        };
        Node* last;

    public:
        int get() {
            if (last == nullptr) {
                return -1;
            }

            int number = last->value;
            Node* oldLast = this->last;
            this->last = this->last->prev;
            delete oldLast;
            return number;
        }
        void insert(int n) {
            if (last == nullptr) {
                last = new Node();
                this->last->value = n;
                return;
            }
            Node* newNode = new Node;
            newNode->value = n;
            newNode->prev = this->last;
            this->last = newNode;
        }
    };

    Stack* lastIndex;

public:
    int select();
    bool search(int);
    void insert(int);
    Structure128(int);
};


#endif //SORTOWANIE_STRUCTURE128_H
