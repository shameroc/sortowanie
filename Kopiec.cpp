#include "Kopiec.h"

void Kopiec::heapify(int* arr, int i, int n) {
    int lewy_i = Kopiec::potomek_lewy(i);
    int prawy_i = Kopiec::potomek_prawy(i);
    int najwiekszy = i;

    if (lewy_i < n && arr[lewy_i] > arr[najwiekszy]) {
        najwiekszy = lewy_i;
    } else {
        najwiekszy = i;
    }

    if (prawy_i < n && arr[prawy_i] > arr[najwiekszy]) {
        najwiekszy = prawy_i;
    }

    if (najwiekszy != i) {
        std::swap(arr[i], arr[najwiekszy]);
        Kopiec::heapify(arr, najwiekszy, n);
    }
}

void Kopiec::build_heap(int * arr, int n) {
    for (int i = (n / 2) - 1; i >= 0; i--) {
        Kopiec::heapify(arr, i, n);
    }
}

int Kopiec::potomek_lewy (int n) {
    return n << 1;
}

int Kopiec::potomek_prawy (int n) {
    return ( n << 1 ) + 1;
}