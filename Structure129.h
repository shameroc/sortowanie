//
// Created by sh4 on 04.01.2021.
//

#ifndef SORTOWANIE_STRUCTURE129_H
#define SORTOWANIE_STRUCTURE129_H


class Structure129 {
private:
    struct List {
        struct Node {
            Node* prev;
            Node* next;
            int value;
        };
        Node* frontElement;
        int deleteFrontElement() {
            if (frontElement == nullptr) return -1;
            int value = frontElement->value;
            List::Node* frontElementCpy = frontElement;
            frontElement = frontElement->next;
            delete frontElementCpy;
            if (frontElement != nullptr) {
                frontElement->prev = nullptr;
            }
            return value;
        }

        Node* insertFrontElement(int value) {
            Node* newNode = new Node();
            newNode->value = value;

            if (frontElement == nullptr) {
                frontElement = newNode;
                return newNode;
            }
            Node* frontElementCopy = frontElement;

            frontElement = newNode;
            newNode->next = frontElementCopy;
            frontElementCopy->prev = newNode;
            return newNode;
        }

        void deleteNode(Node* node) {
            if (node == nullptr) return;
            if (node->next == nullptr && node->prev == nullptr) {
                delete node;
                this->frontElement = nullptr;
                return;
            }
            if (node->next != nullptr && node->prev == nullptr) {
                node->prev->next = nullptr;
                delete node;
                return;
            }
            if (node->next == nullptr && node->prev != nullptr) {
                deleteFrontElement();
                return;
            }

            node->prev->next = node->next;
            node->next->prev = node->prev;
            delete node;
        }
    };

    struct DoubleArray {
        bool isPresent;
        List::Node* node;
    };

    DoubleArray* arr;
    List* list;
    int n;
public:
    void push(int);
    void pop();
    bool search(int);
    void remove(int);
    Structure129(int);
};


#endif //SORTOWANIE_STRUCTURE129_H
