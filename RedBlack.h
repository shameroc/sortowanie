//
// Created by sh4 on 03.01.2021.
//

#ifndef SORTOWANIE_REDBLACK_H
#define SORTOWANIE_REDBLACK_H

#include "Tree.h"

class RedBlack : public Tree {
private:
    Bst* bst;
    void fixInsert(Tree::Node*);
    void fixRemove(Tree::Node*);
    void left_rotate(Tree::Node*);
    void right_rotate(Tree::Node*);
    void rbTransplant(Tree::Node*, Tree::Node*);
    void remove(Tree::Node*, int);

public:
    bool search (int);
    void insert (int);
    void remove (int);
    void print();
    RedBlack(int);
    RedBlack();
};


#endif //SORTOWANIE_REDBLACK_H
