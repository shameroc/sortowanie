#include <iostream>
#include "Sort.cpp"
#include "Bst.cpp"
#include "Avl.cpp"
#include "RedBlack.cpp"
#include "Structure128.cpp"
#include "Structure129.cpp"
#include "Structure130.cpp"

void test_sort ();
void test_bst ();
void test_avl ();
void test_redBlack ();
void test_structure128 ();
void test_structure129();
void test_structure130();

int main() {
//    test_avl();
//    test_bst();
//    test_redBlack();
//    test_structure128();
//    test_structure129();
    test_structure130();
    return 0;
}

void test_sort () {
    int arr[12] = {3, 4, 14, -4, 234, 432, -23, 11, 42, 34, 24, 3};
    Sort::heapSort(arr, 12);
    Sort::check(arr, 12);
    Sort::print(arr, 12);
}

void test_bst () {
    std::cout << "/------------------------------/\nBST\n/------------------------------/\n";
    Tree* tree = new Bst();
    tree->insert(15);
    tree->insert(12);
    tree->insert(43);
    tree->insert(13);
    tree->insert(16);
    tree->insert(177);
    tree->insert(31);
    tree->insert(44);
    tree->print();

    std::cout << '\n';

    tree->remove(13);
    tree->print();

    tree->remove(177);


    std::cout << '\n';
    tree->print();
    std::cout << '\n';

}

void test_avl () {
    std::cout << "/------------------------------/\nAVL\n/------------------------------/\n";
    Tree* tree = new Avl();
    tree->insert(15);
    tree->insert(12);
    tree->insert(43);
    tree->insert(13);
    tree->insert(16);
    tree->insert(177);
    tree->insert(31);
    tree->insert(44);
    tree->print();

    std::cout << '\n';

    tree->remove(13);
    tree->print();

    tree->remove(177);


    std::cout << '\n';
    tree->print();
    std::cout << '\n';
}

void test_redBlack () {
    std::cout << "/------------------------------/\nRED BLACK\n/------------------------------/\n";
    Tree* tree = new RedBlack();
//    std::cout << "fsdafdsa";
//    tree->print();
    tree->insert(15);
    tree->insert(12);
    tree->insert(43);
    tree->insert(13);
    tree->insert(16);
    tree->insert(177);
    tree->insert(31);
    tree->insert(44);
    tree->print();

//
//    std::cout << '\n';
//
//    tree->print();
//
//    std::cout << '\n';
//    tree->print();
//    std::cout << '\n';
}

void test_structure128 () {
    int n = 128;
    Structure128* structure128 = new Structure128(n);
    structure128->insert(4);
    structure128->insert(7);
    structure128->insert(3);
    structure128->insert(1);
    structure128->insert(9);
    std::cout << structure128->select() << " ";
    std::cout << structure128->select() << " ";
    std::cout << structure128->select() << " ";
    std::cout << structure128->search(3) << " ";
    std::cout << structure128->search(7) << " ";
    std::cout << structure128->search(12) << " ";
};

void test_structure129 () {
    int n = 128;
    Structure129* structure129 = new Structure129(n);
    structure129->push(4);
    structure129->push(3);
    structure129->push(1);
    structure129->push(8);
    structure129->push(9);
    structure129->push(10);
    std::cout << structure129->search(4) << " ";
    std::cout << structure129->search(9) << " ";
    std::cout << structure129->search(10) << " ";
    structure129->pop();
    structure129->pop();
    structure129->pop();
    std::cout << structure129->search(9) << " ";
    std::cout << structure129->search(10) << " ";
    std::cout << structure129->search(4) << " ";
    structure129->remove(4);
    std::cout << structure129->search(4) << " ";
};

void test_structure130 () {
    int n = 128;
    Structure130* structure130 = new Structure130();
    structure130->push(4);
    structure130->push(3);
    structure130->push(1);
    structure130->push(8);
    structure130->push(9);
    structure130->push(10);
    structure130->print();
    std::cout << "\n";
//    structure130->pop();
//    structure130->pop();
//    structure130->pop();
//    structure130->print();
//    std::cout << "\n";
    structure130->upToMin();
    structure130->print();
    std::cout << "\n";

};
