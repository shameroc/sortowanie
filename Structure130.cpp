//
// Created by sh4 on 04.01.2021.
//

#include "Structure130.h"

void Structure130::pop() {
    this->list->deleteFrontElement();
}

void Structure130::upToMin() {
    this->list->upToMin();
}

void Structure130::push(int value) {
    this->list->insertFrontElement(value);
}

void Structure130::print() {
    this->list->print();
}

Structure130::Structure130() {
    this->list = new Structure130::List();
}