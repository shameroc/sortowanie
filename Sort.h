#ifndef CWICZENIA_SORT_H
#define CWICZENIA_SORT_H

class Sort {
public:
    static void insertionSort (int*, int);

    static void scalanie (int*, int, int, int);

    static void sortowanie_przez_scalanie (int*, int, int);

    static void heapSort(int*, int);

    static bool check (int*, int);

    static void print (int*, int);
};

#endif
