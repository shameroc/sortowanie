//
// Created by sh4 on 01.12.2020.
//

#ifndef SORTOWANIE_TREE_H
#define SORTOWANIE_TREE_H

class Tree {
protected:
    struct Node {
        int value;
        Node* parent;
        Node* left;
        Node* right;
        int height;
        int color;
    };

public:
    virtual bool search (int) = 0;
    virtual void insert (int) = 0;
    virtual void remove (int) = 0;
    virtual void print () = 0;
};


#endif //SORTOWANIE_TREE_H
