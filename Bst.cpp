//
// Created by sh4 on 27.11.2020.
//

#include "Bst.h"

Bst::Bst(int value) {
    this->root = new Tree::Node();
    this->root->value = value;
}

Bst::Bst() { }


Tree::Node* Bst::insert(Tree::Node* node, Tree::Node* nodeToInsert) {
    if (node == nullptr) {
        return nodeToInsert;
    } else if (nodeToInsert->value > node->value) {
        node->right = Bst::insert(node->right, nodeToInsert);
        return node;
    } else {
        node->left = Bst::insert(node->left, nodeToInsert);
        return node;
    }
}

Tree::Node* Bst::insertIncludingParent(Tree::Node* node, Tree::Node* nodeToInsert, Tree::Node* parent = nullptr) {
    if (node == nullptr) {
        nodeToInsert->parent = parent;
        return nodeToInsert;
    } else if (nodeToInsert->value > node->value) {
        node->right = insertIncludingParent(node->right, nodeToInsert, node);
    } else {
        node->left = insertIncludingParent(node->left, nodeToInsert, node);
    }
    return node;
}

Tree::Node* Bst::search(Tree::Node* node, int value) {
    if (node == nullptr || value == node->value) return node;
    if (value > node->value) return search(node->right, value);
    return search(node->left, value);
}

Tree::Node* Bst::remove (Tree::Node* node, int value) {
    if (node == nullptr) return node;

    if (value < node->value) {
        node->left = remove(node->left, value);
    } else if (value > node->value) {
        node->right = remove(node->right, value);
    } else {
        if (node->left == nullptr) {
            Tree::Node* temp = node->right;
            delete node;
            return temp;
        } else if (node->right == nullptr) {
            Tree::Node* temp = node->left;
            delete node;
            return temp;
        }

        Tree::Node* temp = node->right;
            
        while (temp->left != nullptr) {
            temp = temp->left;
        }

        node->value = temp->value;

        node->right = remove(node->right, temp->value);
        }
    return node;
}

void Bst::inorder (Tree::Node* node) {
    if (node == nullptr) return;
    inorder(node->left);
    std::cout << node->value << " ";
    inorder(node->right);
}

bool Bst::search(int value) {
    if (search(this->root, value) == nullptr) return false;
    return true;
}

void Bst::insert(int value) {
    if (this->root == nullptr) {
        this->root = new Tree::Node();
        this->root->value = value;
        return;
    }
    Tree::Node* node = new Node();
    node->value = value;

    Bst::insert(this->root, node);
}

void Bst::print() {
    Bst::inorder(this->root);
}

void Bst::remove(int value) {
    Bst::remove(this->root, value);
}